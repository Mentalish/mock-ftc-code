//include external libraries
#include <TELEOP.h>
#include <PRIZM.h>

//create needed objects
PRIZM prizm;
PS4 ps4; 
EXPANSION exc;

//motors
int lmotor = 1;
int rmotor = 2;
int rack = 1;
int arm = 2;
int claw = 2;

void resetMotor(){
	exc.setMotorPower(1,arm, 0);
	exc.setMotorPower(1,rack, 0);
}

void setup(){
prizm.PrizmBegin();
prizm.setMotorInvert(rmotor,1);
ps4.setDeadZone(LEFT, 10);
ps4.setDeadZone(RIGHT, 10);
prizm.setServoSpeed(claw, 30);
Serial.begin(115200);
}

void loop(){

	ps4.getPS4();	

	if (ps4.Connected == 1){	

	prizm.setRedLED(LOW);

	resetMotor();

	int lana = ps4.Motor(LY);
	int rana = ps4.Motor(RY);

	prizm.setMotorPower(lmotor,lana);
	prizm.setMotorPower(rmotor,rana);
		if(ps4.Button(L1) == 1){
			exc.setMotorPower(1,rack, 60);
		}
		if(ps4.Button(L2) == 1){
			exc.setMotorPower(1,rack, -60);
		}
		if(ps4.Button(R1) == 1){
			exc.setMotorPower(1,arm, 40);
		}
		if(ps4.Button(R2) == 1){
			exc.setMotorPower(1,arm, -40);
		}
		if(ps4.Button(CROSS) == 1){
			prizm.setServoPosition(claw,40);
		}
		if(ps4.Button(CIRCLE) == 1){
			prizm.setServoPosition(claw,140);
		}
	}
	else {
	ps4.resetTeleOp();
	prizm.setRedLED(HIGH);
	}

}
